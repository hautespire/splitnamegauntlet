//// catchMain.cpp
// This file exists solely for Catch to define a main(), which should happen only once

#define CATCH_CONFIG_MAIN
#include "catch.hpp"
